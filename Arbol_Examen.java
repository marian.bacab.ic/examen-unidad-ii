package arbol_examen;

/**
 *
 * @author Marian
 */
public class Arbol_Examen {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int dato;
        String nombre;
        ArbolBinario_Examen miArbol = new ArbolBinario_Examen();
        miArbol.agregarNodo(70, "F");
        miArbol.agregarNodo(82, "R");
        miArbol.agregarNodo(73, "I");
        miArbol.agregarNodo(68, "D");
        miArbol.agregarNodo(65, "A");
        miArbol.agregarNodo(77, "M");
        miArbol.agregarNodo(78, "N");
        miArbol.agregarNodo(66, "B");
        miArbol.agregarNodo(67, "C");
       

        System.out.println("InOrden");
        if (!miArbol.estaVacio()) {
            miArbol.inOrden(miArbol.raiz);
        }
   
    
    
    
