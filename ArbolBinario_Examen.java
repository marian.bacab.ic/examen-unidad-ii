package arbol_examen;

/**
 *
 */
public class ArbolBinario_Examen {
    
NodoArbol_Examen raiz;

    public ArbolBinario_Examen() {
        raiz = null;
    }

    public void agregarNodo(int d, String nom) {
        NodoArbol_Examen nuevo = new NodoArbol_Examen(d, nom);
        if (raiz == null) {
            raiz = nuevo;
        } else {
            NodoArbol_Examen auxiliar = raiz;
            NodoArbol_Examen padre;
            while (true) {
                padre = auxiliar;
                if (d < auxiliar.dato) {
                    auxiliar = auxiliar.hijoizquierdo;
                    if (auxiliar == null) {
                        padre.hijoizquierdo = nuevo;
                        return;
                    }
                } else {
                    auxiliar = auxiliar.hijoderecho;
                    if (auxiliar == null) {
                        padre.hijoderecho = nuevo;
                        return;
                    }
                }
            }
        }
    }

    public boolean estaVacio() {
        return raiz == null;
    }

    public void inOrden(NodoArbol_Examen r) {       
        if (r != null) {
            inOrden(r.hijoizquierdo);
            System.out.println(r.dato);
            inOrden(r.hijoderecho);
        }
    }

  
