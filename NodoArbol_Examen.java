package arbol_examen;

/**
 *
 * @author Marian
 */
public class NodoArbol_Examen {
   int dato;
    String nombre;
    NodoArbol_Examen hijoizquierdo, hijoderecho;
    public NodoArbol_Examen (int d, String s){
        this.dato = d;
        this.nombre = s;
        this.hijoizquierdo = null;
        this.hijoderecho = null;
    }
    public String toString(){
        return nombre + "El dato es : "+dato;
    }
}